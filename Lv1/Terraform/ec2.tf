resource "aws_instance" "nginx" {
  // ami           = "ami-0742b4e673072066f" # us-east-1
  // ami-048f6ed62451373d9
  ami                         = "ami-0affd4508a5d2481b" # us-east-1
  instance_type               = "t2.micro"
  vpc_security_group_ids      = [aws_security_group.allow_http.id]
  key_name                    = "sumanth"
  associate_public_ip_address = true
  subnet_id                   = "subnet-ec2320a1"

  credit_specification {
    cpu_credits = "unlimited"
  }
  tags = {
    Terraform   = "true"
    Environment = "dev"
    Role        = "nginx"
  }
}
resource "aws_security_group" "allow_http" {
  name        = "allow_http"
  description = "Allow HTTP inbound traffic"
  vpc_id      = "vpc-a11b97dc"

  ingress {
    description = "HTTP from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["172.31.0.0/16"]
  }

  ingress {
    description = "HTTP from Anywhere"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # your local ip
  }

  ingress {
    description = "SSH from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # your local ip
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_http"
  }
}